package com.example.demo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("cliente")
public class TesteController {

    @Autowired
    private ClienteRepository clienteRepository;

    @GetMapping(path = "v1")
    public ClienteDto buscarCliente(@PathVariable Long id){

        Optional<ClienteDto> cliente = clienteRepository.findById(id);
        if(!cliente.isPresent()){
            ResponseEntity.notFound().build();
        }

        return cliente.get();
    }

    @ApiOperation(
            value="Cadastrar uma nova pessoa",
//			response=ResponseModel.class,
            notes="Essa operação salva um novo registro com as informações de pessoa.")
    @ApiResponses(value= {
            @ApiResponse(
                    code=200,
                    message="Retorna um ResponseModel com uma mensagem de sucesso"
//					,response=ResponseModel.class
            ),
            @ApiResponse(
                    code=500,
                    message="Caso tenhamos algum erro vamos retornar um ResponseModel com a Exception"
//					,response=ResponseModel.class
            )
    })
    @GetMapping(path = "v1/clientes")
    public List<ClienteDto> listar(){
        List<ClienteDto> listaCliente = clienteRepository.findAll();
        if(CollectionUtils.isEmpty(listaCliente)){
            ResponseEntity.notFound().build();
        }
        return listaCliente;
    }

    @PostMapping(path = "v1")
    public ResponseEntity<ClienteDto> salvar(@RequestBody @Valid ClienteDto clienteDto, UriComponentsBuilder uriBuilder){

        clienteDto = clienteRepository.save(clienteDto);
        URI uri = uriBuilder.path("/cliente/{id}").buildAndExpand(clienteDto.getId()).toUri();
        return ResponseEntity.created(uri).body(clienteDto);
    }

    @PutMapping(path = "v1")
    public ResponseEntity<ClienteDto> atualizar(@RequestBody ClienteDto clienteDto){

        clienteDto = clienteRepository.save(clienteDto);
        return ResponseEntity.ok(clienteDto);
    }

    @DeleteMapping(path = "v1/{id}")
    public ResponseEntity<ClienteDto> remover(@PathVariable Long id){

        clienteRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @PatchMapping(value = "/v1/{id}")
    public ResponseEntity<?> partialUpdateGeneric(
            @RequestBody Map<String, Object> updates,
            @PathVariable("id") String id) {

        ClienteDto clienteDto = (ClienteDto) clienteRepository.save(updates, id);
        return ResponseEntity.ok();
    }



}
